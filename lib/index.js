'use strict';

var Joi = require('joi');
var _ = require('lodash');
var util = require('util');

var expressValidationError = function (errors, options) {
  this.name = 'expressValidationError'
  this.code = 'InvalidContent',
  this.message = 'Validation failed';
  Error.call(this);
  this.errors = errors;
  this.flatten = options.flatten;
  this.statusCode = options.statusCode;
  this.statusText = options.statusText;
  this.stack = (new Error()).stack;
};

expressValidationError.prototype = Object.create(Error.prototype);

expressValidationError.prototype.constructor = expressValidationError

expressValidationError.prototype.toString = function () {
  return JSON.stringify(this.toJSON());
};

expressValidationError.prototype.toJSON = function () {
  var response = {
    code : this.code,
    message : this.message,
    errors : this.errors
  };

  if (this.flatten){
    response = _.flatten(_.pluck(this.errors, 'messages'));
  }

  return response;
};

var defaultOptions = {
  flatten: false,
  stripUnknownHeaders: false,
  stripUnknownBody: false,
  stripUnknownQuery: false,
  stripUnknownParams: false,
  statusCode: 422,
  statusText: 'Bad Request'
};
var globalOptions = {};

exports = module.exports = function (schema) {
  if (!schema) {
    throw new Error("Please provide a validation schema");
  }

  return function (req, res, next)  {

    var validate = function(current, request, schema, location, stripUnknown){

      if (!request[location] || !schema) {
        return;
      }

      Joi.validate(request[location], schema, {abortEarly : false, allowUnknown: true, stripUnknown : !!stripUnknown}, function(errors, value){
        if (!errors || errors.details.length === 0) {
          request[location] = value;
          return;
        }

        _.uniq(_.each(errors.details, function(error){
          var errorExists = _.find(current, function(item){
            if (item && item.field === error.path && item.location === location) {
              item.messages.push(error.message);
              return item;
            }
            return;
          });

          if (!errorExists) {
            var type = 'invalid';
            if(error.message.indexOf('is required') !== -1){
              type = 'missing_field';
            }
            current.push({path : error.path, type : type, message : error.message});
          }

        }, errors));
      });
    };

    var errors = [];

    var pushErrors = function(errors, error){
      if (error) {
        errors.push(error);
      }
    };

    // Set default options
    var options = schema.options ? _.defaults({}, schema.options, globalOptions, defaultOptions) : _.defaults({}, globalOptions, defaultOptions);
    pushErrors(errors, validate(errors, req, schema.headers, 'headers', options.stripUnknownHeaders));
    pushErrors(errors, validate(errors, req, schema.body, 'body', options.stripUnknownBody));
    pushErrors(errors, validate(errors, req, schema.query, 'query', options.stripUnknownQuery));
    pushErrors(errors, validate(errors, req, schema.params, 'params', options.stripUnknownParams));

    if (errors && errors.length === 0) {
      return next();
    }

    return next(new expressValidationError(errors, options));
  };
};

exports.expressValidationError = expressValidationError;

exports.options = function (opts) {
  if (!opts) {
    globalOptions = {};
    return;
  }

  globalOptions = _.extend({}, globalOptions, opts);
};
